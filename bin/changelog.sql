TRUNCATE public.product_images RESTART IDENTITY;
TRUNCATE public.jewelry RESTART IDENTITY;
TRUNCATE public.product RESTART IDENTITY;
TRUNCATE public.wishlist RESTART IDENTITY;
TRUNCATE public.wholesale_items RESTART IDENTITY;
TRUNCATE public.transaction RESTART IDENTITY;
TRUNCATE public.product_map RESTART IDENTITY;
TRUNCATE public.order_product_option RESTART IDENTITY;
TRUNCATE public.ring_material RESTART IDENTITY;
TRUNCATE public.address RESTART IDENTITY;
TRUNCATE public.page RESTART IDENTITY;
TRUNCATE public.coupon RESTART IDENTITY;
TRUNCATE public.user_favorite RESTART IDENTITY;
TRUNCATE public.wedding_band RESTART IDENTITY;
TRUNCATE public.order_products RESTART IDENTITY;
TRUNCATE public.category RESTART IDENTITY;
TRUNCATE public.ring RESTART IDENTITY;
TRUNCATE public.wholesale RESTART IDENTITY;
TRUNCATE public.banner RESTART IDENTITY;
TRUNCATE public.shipping_method RESTART IDENTITY;
TRUNCATE public.payment_profiles RESTART IDENTITY;
TRUNCATE public.contact RESTART IDENTITY;
TRUNCATE public.product_categories RESTART IDENTITY;
TRUNCATE public.credit RESTART IDENTITY;
TRUNCATE public.order RESTART IDENTITY;

CREATE SEQUENCE public.question_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE public.question
(
    id integer NOT NULL DEFAULT nextval('question_id_seq'::regclass),
    active smallint NOT NULL DEFAULT 1,
    insert_time timestamp without time zone NOT NULL DEFAULT now(),
    text character varying COLLATE pg_catalog."default",
    failure_text character varying COLLATE pg_catalog."default",
    image character varying COLLATE pg_catalog."default",
    CONSTRAINT question_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

CREATE SEQUENCE public.answer_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE public.answer
(
    id integer NOT NULL DEFAULT nextval('answer_id_seq'::regclass),
    active smallint NOT NULL DEFAULT 1,
    insert_time timestamp without time zone NOT NULL DEFAULT now(),
    text character varying COLLATE pg_catalog."default",
    failure_text character varying COLLATE pg_catalog."default",
    image character varying COLLATE pg_catalog."default",
    CONSTRAINT answer_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

-- September 20

ALTER TABLE public.answer ADD COLUMN question_id INTEGER;
ALTER TABLE public.answer ADD COLUMN featured_image character varying COLLATE pg_catalog."default";
ALTER TABLE public.answer DROP COLUMN failure_text;
ALTER TABLE public.answer DROP COLUMN image;

-- September 21

ALTER TABLE public.question ADD COLUMN correct_answer_id INTEGER;

-- September 25

ALTER TABLE public.question ADD COLUMN display_order INTEGER;
ALTER TABLE public.answer ADD COLUMN display_order INTEGER;

-- September 27

ALTER TABLE public.contact DROP COLUMN phone;
ALTER TABLE public.contact ADD COLUMN company character varying;

-- October 5, 2018

CREATE TABLE public.entries
(
    id SERIAL primary key,
    active SMALLINT NOT NULL DEFAULT 1,
    insert_time TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
    email CHARACTER VARYING COLLATE pg_catalog."default",
    prize CHARACTER VARYING COLLATE pg_catalog."default",
    winner SMALLINT DEFAULT 0
)