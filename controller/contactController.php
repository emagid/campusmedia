<?php

class contactController extends siteController
{

    public function index(Array $params = [])
    {

        $this->configs['Meta Title'] = "Contact | nielsen sports";
        $this->loadView($this->viewData);
    }

    public function index_post()
    {
        $obj = \Model\Contact::loadFromPost();
        if($obj->save()){
            $n = new \Notification\MessageHandler('We will contact you shortly.');
            $_SESSION["notification"] = serialize($n);
        }

        redirect('/');
    }

}