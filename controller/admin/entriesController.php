<?php

class entriesController extends adminController {
	
	function __construct(){
		parent::__construct("Entry", "entries");
	}
	
	function index(Array $params = []){
		$this->_viewData->hasCreateBtn = true;
		parent::index($params);
	}

	function update(Array $arr = []){
		
		parent::update($arr);
	}

	public function exportEmailLists(Array $params = []){
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=campus_media_event_email_list.csv');
        
        $sql="SELECT DISTINCT ON (email) * FROM entries WHERE active=1";

        $users = \Model\Entry::getList(['sql'=>$sql]);
        $output = fopen('php://output', 'w');
        $t=array("No.",'Email');
        fputcsv($output, $t);
        $row ='';

        foreach($users as $key=>$user) {

            $row = array($key+1,$user->email);
            fputcsv($output, $row);  
        }
    }

    public function search()
    {

        $entries = \Model\Entry::search($_GET['keywords']);
        
        echo '[';
        foreach ($entries as $key => $entry) {
        	$winner = $entry->winner == 1 ? 'Won':'Not Selected';
            echo '{ "id": "' . $entry->id . '", "email": "' . $entry->email . '", "prize":"'.$entry->prize.'", "winner":"'.$winner.'" }';
            if ($key < (count($entries) - 1)) {
                echo ",";
            }
        }
        echo ']';
    }
  
}