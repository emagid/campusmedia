<?php

class view_360Controller extends siteController {
        function __construct(){
        parent::__construct();
    }    
    public function index(Array $params = []){


        $this->configs['Meta Title'] = "Campus Media | Spin and Win";
        // $this->configs['Meta Description'];
        
        $this->loadView($this->viewData);
    }

    public function createEntry(){
    	$resp['status'] = false;
    	$user_entry = \Model\Entry::getItem(null,['where'=>"email = '".$_POST['email']."'"]);
    	if($user_entry){
    		if($user_entry->prize != ''){
	    		$resp['status'] = false;
	    		$resp['msg'] = "already entered";
	    	} else {
	    		$resp['status'] = true; 
	    		$resp['email'] = $user_entry->email;
	    	}
    	} else {
	    	$new_entry = new \Model\Entry();
	    	$new_entry->email = $_POST['email'];
	    	if($new_entry->save()){
	    		$resp['status'] = true;
	    		$resp['email'] = $new_entry->email;
	    	} else {
	    		$resp['status'] = false;
	    	}
	    }
    	$this->toJson($resp);
    }

    public function submit_entry(){
    	$resp['status'] = false;
    	$user_entry = \Model\Entry::getItem(null,['where'=>"email = '".$_POST['email']."'"]);
    	$prize = $_POST['prize'];
    	$max_count = 0;
    	$prize_count = \Model\Entry::getCount(['where'=>"prize = '".$prize."'"]);
    	if($user_entry){
    		$user_entry->prize = $prize;
    		if($user_entry->save()){
    			if($prize == \Model\Entry::$prizes[0])
    				$max_count = 90; //90
    			if($prize == \Model\Entry::$prizes[1])
    				$max_count = 80; //80
    			if($prize == \Model\Entry::$prizes[2])
    				$max_count = 60; //60
    			if($prize == \Model\Entry::$prizes[3])
    				$max_count = 80; //80
    			if($prize == \Model\Entry::$prizes[4])
    				$max_count = 40;
    			if($prize == \Model\Entry::$prizes[5])
    				$max_count = 40;

    			if($prize_count <= $max_count){
    				$user_entry->winner = 1;
    				$user_entry->save();
    				$resp['status'] = true;
    			} else {
    				$resp['status'] = false;
    				$resp['msg'] = "prize count exceed";
    			}
    		}
    	} else {
    		$resp['status'] = false;
    		$resp['msg'] = "entry doesn't exist";
    	}
    	$this->toJson($resp);
    }
}