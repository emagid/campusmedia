<?php
 if(count($model->entries)>0): ?>

  <div class="search_bar">
     <div class="topnav">
         <input type="text" id='search' placeholder="Search by name or email" />
         <button type="button">
             <img src="<?=ADMIN_IMG?>search.png" />
         </button>

     </div>
  </div>

  <div class="box box-table">
    <table class="table" id="data-list">
      <thead>
        <tr>
          <th width="30%">Email</th>
          <th width="30%">Prize</th>
          <th width="10%">Winning Status</th>
          <th width="15%" class="text-center">Edit</th>	
          <th width="15%" class="text-center">Delete</th>	
        </tr>
      </thead>
      <tbody>
      <?php foreach($model->entries as $obj){ ?>
      <? //var_dump($obj); exit; ?>
        <tr class="allEntries">
          <td><a href="<?php echo ADMIN_URL; ?>entries/update/<?= $obj->id ?>"><?php echo $obj->email; ?></a></td>
          <td><a href="<?php echo ADMIN_URL; ?>entries/update/<?= $obj->id ?>"><?php echo $obj->prize; ?></a></td>
          <td><a href="<?php echo ADMIN_URL; ?>entries/update/<?= $obj->id ?>"><?= $obj->winner == 1 ? 'Won':'Not Selected' ?></a></td>
            
          <td class="text-center">
            <a class="btn-actions" href="<?= ADMIN_URL ?>entries/update/<?= $obj->id ?>">
              <i class="icon-pencil"></i> 
            </a>
          </td>
          <td class="text-center">
            <a class="btn-actions" href="<?= ADMIN_URL ?>entries/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
              <i class="icon-cancel-circled"></i> 
            </a>
          </td>
        </tr>
      <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'entries';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>

<script type="text/javascript">
    $("#search").keyup(function () {
        var url = "<?php echo ADMIN_URL; ?>entries/search";
        var keywords = $(this).val();
        if (keywords.length > 2) {
            $.get(url, {keywords: keywords}, function (data) {
                $("#data-list tbody tr").not('.allEntries').remove();
                $('.paginationContent').hide();
                $('.allEntries').hide();

                var list = JSON.parse(data);

                for (key in list) {
                    var tr = $('<tr />');
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>entries/update/' + list[key].id).html(list[key].email));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>entries/update/' + list[key].id).html(list[key].prize));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>entries/update/' + list[key].id).html(list[key].winner));
                    var editTd = $('<td />').addClass('text-center').appendTo(tr);
                    var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>entries/update/' + list[key].id);
                    var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
                    var deleteTd = $('<td />').addClass('text-center').appendTo(tr);
                    var deleteLink = $('<a />').appendTo(deleteTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>entries/delete/' + list[key].id);
                    deleteLink.click(function () {
                        return confirm('Are You Sure?');
                    });
                    var deleteIcon = $('<i />').appendTo(deleteLink).addClass('icon-cancel-circled');

                    tr.appendTo($("#data-list tbody"));
                }

            });
        } else {
            $("#data-list tbody tr").not('.allEntries').remove();
            $('.paginationContent').show();
            $('.allEntries').show();
        }
    });

</script>