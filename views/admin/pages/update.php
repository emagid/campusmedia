<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->page->id ?>"/>

    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
            <li role="presentation"><a href="#content-tab" aria-controls="content" role="tab" data-toggle="tab">Page Content</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general-tab">
                <div class="row">
                    <div class="col-md-16">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>Title</label>
                                <?php echo $model->form->editorFor("title"); ?>
                            </div>
                            <div class="form-group">
                                <label>Slug</label>
                                <?php echo $model->form->editorFor("slug"); ?>
                            </div>
                            <div class="form-group">
                                <label>Content</label>
                                <?php echo $model->form->textAreaFor("description", ["class" => "ckeditor"]); ?>
                            </div>
                        </div>
                        </div>

                    <div class="col-md-8">
                        <div class="box">
                            <h4>Meta Information</h4>

                            <div class="form-group">
                                <label>Featured image</label>

                                <p>
                                    <small>(ideal featured image size is 1920 x 300)</small>
                                </p>
                                <?php
                                $img_path = "";
                                if ($model->page->featured_image != "") {
                                    $img_path = UPLOAD_URL . 'pages/' . $model->page->featured_image;
                                }
                                ?>
                                <p><input type="file" name="featured_image" class='image'/></p>
                                <?php if ($model->page->featured_image != "") { ?>
                                    <div class="well well-sm pull-left">
                                        <img src="<?php echo $img_path; ?>" width="100"/>
                                        <br/>
                                        <a href="<?= ADMIN_URL . 'pages/delete_image/' . $model->page->id . '/?featured_image=1'; ?>"
                                           class="btn btn-default btn-xs">Delete</a>
                                    </div>
                                <?php } ?>
                                <div id='preview-container'></div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <label>Meta title</label>
                                <?php echo $model->form->editorFor("meta_title"); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta keywords</label>
                                <?php echo $model->form->textAreaFor("meta_keywords"); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta description</label>
                                <?php echo $model->form->textAreaFor("meta_description", ["rows" => "3"]); ?>
                            </div>
                            <!--div class="checkbox form-group">
                                <label>
                                    <?php echo $model->form->checkBoxFor("active", 1); ?>  Active?
                                </label>
                            </div-->
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="content-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div id="content_box" class="box">
                            <h4>Content Header and Description</h4>
                            <a id="add_desc" href="#">Add</a>
                            <? if(($content = $model->page->content) != ''){
                                $val = 0;
                                foreach(json_decode($content) as $key=>$seo){
                                    ?>
                                    <div class="form-group">
                                        <input data-title-id="<?=$val?>" type="text" name="content_title[]" placeholder="Content Title" value="<?=$key?>">
                                        <textarea data-desc-id="<?=$val?>" id="editor_<?=$val?>" class="ckeditor" name="content_description[]" placeholder="Content Description"><?=$seo?></textarea>
                                        <label>Order</label>
                                        <input type="number" name="content_order[]" value="<?=$val?>" placeholder="Enter display order"><br>
                                        <a href="#" class="delete_content">Delete</a>
                                    </div>
                                <?$val++;}?>
                            <?}?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-24">
            <button type="submit" class="btn btn-success btn-lg">Save</button>
        </div>
    </div>
</form>


<?= footer(); ?>


<script type='text/javascript'>
    $(document).ready(function () {

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var img = $("<img />");
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                    $("#preview-container").html(img);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
            $('#previewupload').show();
        });

        $("input[name='title']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-')
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
        var val = <?=isset($val)?$val:0?>;
        $('#add_desc').on('click', function(){
            var content = $(
                '<div class="form-group"><input type="text" name="content_title[]" placeholder="Content Header"><textarea id="editor_'+val+'" class="ckeditor" name="content_description[]"></textarea><label>Order</label><input type="number" name="content_order[]" value="'+val+'" placeholder="Enter display order"><br><a href="#" class="delete_content">Delete</a></div>'
            );
            $('#content_box').append(content);
            triggerCK();
        });

        function triggerCK(){
            var editor = 'editor_'+val;
            CKEDITOR.replace(editor);
            val++;
        }

        //Delete function for removing container <div>. Revise when adding or removing elements
        $(document).on('click','.delete_content', function(){
            $(this).parent().remove();
        });
    });

</script>