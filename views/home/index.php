<main class="home_page">
	<header>
		<img class='banner' src="<?=FRONT_ASSETS?>img/banner.png">
	</header>
    
    <section class="swiper-container loading">
  <div class="swiper-wrapper">
    <div class="swiper-slide" style="background-image:url(<?=FRONT_ASSETS?>img/album_1_mini.jpg)">
      <img src="<?=FRONT_ASSETS?>img/album_1_mini.jpg" class="entity-img" />
    </div>
      <div class="swiper-slide" style="background-image:url(<?=FRONT_ASSETS?>img/album_2_mini.jpg)">
      <img src="<?=FRONT_ASSETS?>img/album_1_mini.jpg" class="entity-img" />
    </div>
      <div class="swiper-slide" style="background-image:url(<?=FRONT_ASSETS?>img/album_3_mini.jpg)">
      <img src="<?=FRONT_ASSETS?>img/album_1_mini.jpg" class="entity-img" />
    </div>
      <div class="swiper-slide" style="background-image:url(<?=FRONT_ASSETS?>img/album_4_mini.jpg)">
      <img src="<?=FRONT_ASSETS?>img/album_1_mini.jpg" class="entity-img" />
    </div>
    <div class="swiper-slide" style="background-image:url(<?=FRONT_ASSETS?>img/album_5_mini.jpg)">
      <img src="https://drive.google.com/uc?export=view&id=0B_koKn2rKOkLNXBIcEdOUFVIWmM" class="entity-img" />
    </div>
  </div>
</section>

	<section class="home">

  <script>
// Params
var sliderSelector = '.swiper-container',
    options = {
      init: false,
      loop: true,
      speed:800,
          autoplay: {
    delay: 3000,
  },
      slidesPerView: 2, // or 'auto'
      // spaceBetween: 10,
      centeredSlides : true,
      effect: 'coverflow', // 'cube', 'fade', 'coverflow',
      coverflowEffect: {
        rotate: 50, // Slide rotate in degrees
        stretch: 0, // Stretch space between slides (in px)
        depth: 100, // Depth offset in px (slides translate in Z axis)
        modifier: 1, // Effect multipler
        slideShadows : true, // Enables slides shadows
      },
      grabCursor: true,
      parallax: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        1023: {
          slidesPerView: 1,
          spaceBetween: 0
        }
      },
      // Events
      on: {
        imagesReady: function(){
          this.el.classList.remove('loading');
        }
      }
    };
var mySwiper = new Swiper(sliderSelector, options);

// Initialize slider
mySwiper.init();     
</script>  
        <style>
        [class^="swiper-button-"], .swiper-container-horizontal > .swiper-pagination-bullets .swiper-pagination-bullet, .swiper-container-horizontal > .swiper-pagination-bullets .swiper-pagination-bullet::before {
  transition: all 0.3s ease;
}

*,
*:before,
*:after {
  box-sizing: border-box;
  margin: 0;
  padding: 0;
}


.swiper-container:hover .swiper-button-prev,
.swiper-container:hover .swiper-button-next {
  -webkit-transform: translateX(0);
          transform: translateX(0);
  opacity: 1;
  visibility: visible;
}

.swiper-slide {
  background-position: center;
  background-size: cover;
}
.swiper-slide .entity-img {
  display: none;
}
.swiper-slide .content {
  position: absolute;
  top: 40%;
  left: 0;
  width: 50%;
  padding-left: 5%;
  color: #fff;
}
.swiper-slide .content .title {
  font-size: 2.6em;
  font-weight: bold;
  margin-bottom: 30px;
}
.swiper-slide .content .caption {
  display: block;
  font-size: 13px;
  line-height: 1.4;
}

[class^="swiper-button-"] {
  width: 44px;
  opacity: 0;
  visibility: hidden;
}

.swiper-button-prev {
  -webkit-transform: translateX(50px);
          transform: translateX(50px);
}

.swiper-button-next {
  -webkit-transform: translateX(-50px);
          transform: translateX(-50px);
}

.swiper-container-horizontal > .swiper-pagination-bullets .swiper-pagination-bullet {
  margin: 0 9px;
  position: relative;
  width: 12px;
  height: 12px;
  background-color: #fff;
  opacity: 0.4;
}
.swiper-container-horizontal > .swiper-pagination-bullets .swiper-pagination-bullet::before {
  content: "";
  position: absolute;
  top: 50%;
  left: 50%;
  width: 18px;
  height: 18px;
  -webkit-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
  border: 0px solid #fff;
  border-radius: 50%;
}
.swiper-container-horizontal > .swiper-pagination-bullets .swiper-pagination-bullet:hover, .swiper-container-horizontal > .swiper-pagination-bullets .swiper-pagination-bullet.swiper-pagination-bullet-active {
  opacity: 1;
}
.swiper-container-horizontal > .swiper-pagination-bullets .swiper-pagination-bullet.swiper-pagination-bullet-active::before {
  border-width: 1px;
}

@media (max-width: 1180px) {
  .swiper-slide .content .title {
    font-size: 25px;
  }
  .swiper-slide .content .caption {
    font-size: 12px;
  }
}
@media (max-width: 1023px) {
  .swiper-container {
    height: 40vw;
  }
  .swiper-container.swiper-container-coverflow {
    padding-top: 0;
  }
}

        </style>

	  <div class="choices album">
	    <a href='/view_360' id='stadium' class="choice click_action">
	      <img src="<?=FRONT_ASSETS?>img/tap.png">
	      <h2>SPIN TO WIN CONTEST</h2>
	    </a>
	  </div>
	</section>
</main>

<script type="text/javascript">
	// opening animations
    $('main h1').fadeIn();

    setTimeout(function(){
        $('main h1').fadeOut(1500);
        $('main').css('background-position', 'center 1180px');
        $('header').fadeIn(1500);
    }, 1500);

    setTimeout(function(){
        $('.choices').fadeIn(2000);
        $('.choices').css('display', 'flex');
        $('.choices').css('margin-top', '150px');
    }, 3000);
</script>

<style>
/*=======================  TIMEOUT   ======================*/




  .header {
    background-color: #02abed;
      padding: 43px 0;
  }

  .timeout img.move {
      position: absolute;
      left: 50%;
      transform: translateX(-50%);
    -webkit-animation: img 4s infinite ease-in-out;
    
  }

  @-webkit-keyframes img {
    10%, 60%  { margin-top: 0; }
      
    40% { margin-top: -120px; }
  }


  .timeout {
    display: none;
    position: fixed;
    height: 100%;
    width: 100%;
    top: 0;
    z-index: 20000;
    background-position: center 1180px;
    background-color: white;
  }

  .timeout2 {
    z-index: 20001;
    background: white;
  }

  .timeout2 .taphere {
    position: absolute;
    top: -300px;
    width: 100%;
  }


  .timeout2 img.move {
    position: absolute;
    top: 282px;
    width: 9%;
  }

  .header h2 {
    text-align: center;
    margin-bottom: 20px;
    font-size: 59px;
    color: white;
  }

  .header p {
    text-align: center;
    font-size: 29px;
    color: white;
  }



/*  =============  HOME PAGE  ==================  */
.home {
  opacity: 0;
  transition: padding-top .5s, opacity .5s;
}

.logo {
  height: 157px;
  position: absolute;
  right: 140px;
}

.title_holder {
  padding-top: 250px;
  text-align: center;
}









.logo_full {
  width: 333px;
  position: absolute;
  left: 50%;
  bottom: 115px;
  -webkit-transform: translateX(-50%);
  transform: translateX(-50%);
}

    .nielsen_text {
    position: absolute;
    font-family: fantasy;
    font-size: 40px;
    color: #00abec;
    font-family: 'Knockout28', 'Arial', 'sans-serif';
    line-height: 1.1;
    left: 242px;
    top: -2px;
}
/**/

</style>