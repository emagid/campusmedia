<header>
    <a href="/">
	   <img class='banner' src="<?=FRONT_ASSETS?>img/banner.png">
    </a>
</header> 

<div class="wheel_container">
    <!-- The Modal -->
    <div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
      <div class='offclick'></div>
<!--      Prize: $10 Gift Card-->
      <div id="giftcard_10" style="display: none;">
        <p>Thank you for playing! You've won a <br><strong>$10 Gift Card</strong></p>
        <img src="<?=FRONT_ASSETS?>img/giftcard.png">
      </div>
      
<!--      Prize: Black Sunglasses-->
      <div id="glasses_blk" style="display:none;">
        <p>Thank you for playing! You've won <br><strong>Black Sunglasses</strong></p>
        <img src="<?=FRONT_ASSETS?>img/black_sunglasses.jpg">
      </div>
      
      <!--      Prize: Pop Socket-->
      <div id="pop_socket" style="display:none;">
        <p>Thank you for playing! You've won a <br><strong>Pop Socket</strong></p>
        <img src="<?=FRONT_ASSETS?>img/popsocket.jpg">
      </div>
      
      <!--      Prize: $10 White Sunglasses-->
      <div id="sunglasses_wht" style="display:none;">
        <p>Thank you for playing! You've won <br><strong>White Sunglasses</strong></p>
        <img src="<?=FRONT_ASSETS?>img/black_sunglasses.png">
      </div>
      
      <!--      Prize: $5 Gift Card-->
      <div id="giftcard_5" style="display:none;">
        <p>Thank you for playing! You've won a <br><strong>$5 Gift Card</strong></p>
        <img src="<?=FRONT_ASSETS?>img/giftcard.png">
      </div>
      
      <!--      Prize: H&M Goodie Bag-->
      <div id="goodie_bag" style="display:none;">
        <p>Thank you for playing! You've won a <br><strong>H&M Goodie Bag</strong></p>
        <img src="<?=FRONT_ASSETS?>img/goodie_bag.JPG">
      </div>
      <!--      Prize: H&M Goodie Bag-->
      <div id="no_email" style="display:none;">
        <p>Thank you for playing! But Email is required to submit your entry</p>
      </div>
      <div id="prize_limit_exceeds" style="display:none;">
        <p></p>
      </div>
  </div>

</div>
        
            
          <section class="contact_form">
      <h1>Enter your email below to play</h1>
      <form id="entry_form">
        <input required id='entry_email' class='jQKeyboard form-control' type="text" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" name="email" placeholder="Email">
        <input id="btn-submit" class='button click_action' type="submit" value='SUBMIT'>
      </form>
    </section>
    <section class="prompt" style="display:none;">
            <h1>Tap spin below for your chance to win!</h1>
    </section>  
        
        <div id="wheel" align="center">
            <input id="user_email" type="hidden" name="user_email" value="">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <div class="power_controls">
                            <br />
                            <br />
                            <a id="spin_button"  alt="Spin" class="choice click_action">
	      <img src="/content/frontend/assets/img/tap.png">
	      <h2>TAP TO SPIN</h2>
	    </a>
                        </div>
                    </td>

                </tr>
                <tr>                    <td width="657" height="873" class="the_wheel" align="center" valign="center">
                        <canvas id="canvas" width="651" height="651">
                            <p style="{color: white}" align="center">Sorry, your browser doesn't support canvas. Please try another.</p>
                        </canvas>
                    </td></tr>
            </table>
        </div>
            </div>

        <script type="text/javascript">
          $('#spin_button').click(function(e){
              var email = $("#entry_email").val();
              if(email == ''){
                $("#myModal").show();
                $("#no_email p").text("Please enter email and click submit to play");
                $("#no_email").show()
                window.setTimeout(function() {
                    window.location.href = '/view_360';
                }, 4000);
              } else {
                $.post("/view_360/createEntry",{email: email}, function(response){
                  if(response.status){
                    $("#user_email").val(response.email);
                    startSpin();
                  } else {
                    if(response.msg == 'already entered'){
                      $("#myModal").show();
                      $("#no_email p").text("You already played..Thankyou!!");
                      $("#no_email").show();
                      $("#wheel").slideUp();
                      window.setTimeout(function() {
                          window.location.href = '/';
                      }, 8000);
                    }
                  }
                })
              }
          })
        </script>

        <script>
            // Create new wheel object specifying the parameters at creation time.
            var theWheel = new Winwheel({
                'numSegments'  : 6,     // Specify number of segments.
                'outerRadius'  : 318,   // Set outer radius so wheel fits inside the background.
                'textFontSize' : 28,    // Set font size as desired.
                'segments'     :        // Define segments including colour and text.
                [
                   {'fillStyle' : '#F58025', 'text' : '$10 GIFT CARD', 'textFillStyle' : '#000'},
                   {'fillStyle' : '#ED1C4F', 'text' : 'BLACK SUNGLASSES', 'textFillStyle' : '#000', 'textFontSize' : 22},
                   {'fillStyle' : '#0293D0', 'text' : 'POP SOCKET', 'textFillStyle' : '#000'},
                   {'fillStyle' : '#F58025', 'text' : 'WHITE SUNGLASSES', 'textFillStyle' : '#000', 'textFontSize' : 22},
                   {'fillStyle' : '#96c93e', 'text' : '$5 GIFT CARD', 'textFillStyle' : '#000'},
                   {'fillStyle' : '#fff200', 'text' : 'H&M GOODIE BAG', 'textFillStyle' : '#000'}
                ],
                'animation' :           // Specify the animation to use.
                {
                    'type'     : 'spinToStop',
                    'duration' : 5,     // Duration in seconds.
                    'spins'    : 8,     // Number of complete spins.
                    'callbackFinished' : alertPrize
                }
            });

            // Vars used by the code in this page to do power controls.
            var wheelPower    = 0;
            var wheelSpinning = false;

            // -------------------------------------------------------
            // Function to handle the onClick on the power buttons.
            // -------------------------------------------------------
            function powerSelected(powerLevel)
            {
                // Ensure that power can't be changed while wheel is spinning.
                if (wheelSpinning == false)
                {
                    // Reset all to grey incase this is not the first time the user has selected the power.
                    document.getElementById('pw1').className = "";
                    document.getElementById('pw2').className = "";
                    document.getElementById('pw3').className = "";

                    // Now light up all cells below-and-including the one selected by changing the class.
                    if (powerLevel >= 1)
                    {
                        document.getElementById('pw1').className = "pw1";
                    }

                    if (powerLevel >= 2)
                    {
                        document.getElementById('pw2').className = "pw2";
                    }

                    if (powerLevel >= 3)
                    {
                        document.getElementById('pw3').className = "pw3";
                    }

                    // Set wheelPower var used when spin button is clicked.
                    wheelPower = powerLevel;

                    // Light up the spin button by changing it's source image and adding a clickable class to it.
//                    document.getElementById('spin_button').src = "spin_on.png";
                    document.getElementById('spin_button').className = "clickable";
                }
            }

            // -------------------------------------------------------
            // Click handler for spin button.
            // -------------------------------------------------------
            function startSpin()
            {
                // Ensure that spinning can't be clicked again while already running.
                if (wheelSpinning == false)
                {
                    // Based on the power level selected adjust the number of spins for the wheel, the more times is has
                    // to rotate with the duration of the animation the quicker the wheel spins.
                    if (wheelPower == 1)
                    {
                        theWheel.animation.spins = 3;
                    }
                    else if (wheelPower == 2)
                    {
                        theWheel.animation.spins = 8;
                    }
                    else if (wheelPower == 3)
                    {
                        theWheel.animation.spins = 15;
                    }

                    // Disable the spin button so can't click again while wheel is spinning.
//                    document.getElementById('spin_button').src       = "spin_off.png";
//                    document.getElementById('spin_button').className = "";

                    // Begin the spin animation by calling startAnimation on the wheel object.
                    theWheel.startAnimation();

                    // Set to true so that power can't be changed and spin button re-enabled during
                    // the current animation. The user will have to reset before spinning again.
                    wheelSpinning = true;
                }
            }

            // -------------------------------------------------------
            // Function for reset button.
            // -------------------------------------------------------
            function resetWheel()
            {
                theWheel.stopAnimation(false);  // Stop the animation, false as param so does not call callback function.
                theWheel.rotationAngle = 0;     // Re-set the wheel angle to 0 degrees.
                theWheel.draw();                // Call draw to render changes to the wheel.

                document.getElementById('pw1').className = "";  // Remove all colours from the power level indicators.
                document.getElementById('pw2').className = "";
                document.getElementById('pw3').className = "";

                wheelSpinning = false;          // Reset to false to power buttons and spin can be clicked again.
            }

            // -------------------------------------------------------
            // Called when the spin animation has finished by the callback feature of the wheel because I specified callback in the parameters
            // note the indicated segment is passed in as a parmeter as 99% of the time you will want to know this to inform the user of their prize.
            // -------------------------------------------------------
            function alertPrize(indicatedSegment)
            {
                // Do basic alert of the segment text. You would probably want to do something more interesting with this information.
                // alert("You have won " + indicatedSegment.text);
                submitEntry(indicatedSegment.text);
            }
        </script>



<script type="text/javascript">
  $("#btn-submit").click(function(e){
    e.preventDefault();
    var email = $("#entry_email").val();
    if(email != ''){
      $.post("/view_360/createEntry",{email: email}, function(response){
        console.log(response);
        if(response.status){
          $("#user_email").val(response.email);
          $("#wheel").slideDown();
        } else {
          if(response.msg == 'already entered'){
            $("#myModal").show();
            $("#no_email p").text("You already played..Thankyou!!");
            $("#no_email").show();
            $("#wheel").slideUp();
            window.setTimeout(function() {
                window.location.href = '/';
            }, 8000);
          }
          // $("#wheel").slideUp();
          //alert("Error..try again!")

        }
      });
    }
  })

</script>



<script>
  
$( "input.button.click_action" ).click(function() {
  $( ".prompt" ).slideDown( "slow", function() {
    // Animation complete.
  });
        $( ".contact_form" ).hide();
});

</script>




<script type="text/javascript">
  function submitEntry(prize){
    var email = document.getElementById("user_email").value
    console.log(email + " - " + prize);
    $.post("/view_360/submit_entry",{email:email,prize:prize},function(data){
      console.log(data);
      if(data.status){
          $("#myModal").show();
          if(prize == '$10 GIFT CARD'){
            $("#giftcard_10").show();
          } 
          if(prize == 'BLACK SUNGLASSES'){
            $("#glasses_blk").show();
          }
          if(prize == 'POP SOCKET'){
            $("#pop_socket").show();
          }
          if(prize == 'WHITE SUNGLASSES'){
            $("#sunglasses_wht").show();
          }
          if(prize == '$5 GIFT CARD'){
            $("#giftcard_5").show();
          }
          if(prize == 'H&M GOODIE BAG'){
            $("#goodie_bag").show();
          }
          
//        alert("You won " + prize);
      } else {
        console.log(data.msg);
        if(data.msg == "entry doesn't exist"){
            $("#myModal").show();
            $("#no_email").show();
//          alert("Entry doesn't exist!");
            
        } else if(data.msg == "prize count exceed"){
            $("#myModal").show();
            $("#prize_limit_exceeds p").text("Sorry, we don't have more "+ prize + ", but you can chose from other prizes we have!");
            $("#prize_limit_exceeds").show();
//          alert("You got " + prize + ", but limit exceeded");
        }
      }
      window.setTimeout(function() {
            window.location.href = '/';
        }, 8000);
    })
  }
</script>