<a href='/' class="button click_action back_home info_home">HOME<img src="<?=FRONT_ASSETS?>img/back_grey.png"></a>  
<img class='logo' src="<?=FRONT_ASSETS?>img/logo.png">
<section class="future_of_sport">
	
	<section class="info">
			<img id='scroll' src="<?=FRONT_ASSETS?>img/back_grey.png">

			<!-- heading text -->
			<div class="info_header">
		       <h1>NIELSEN SPORTS CONNECT</h1>
                <h2>Your key to unlocking the value of sponsorship across platforms & screens</h2>
<!--		       <h2>THE LEADING GLOBAL MEASUREMENT SOLUTION SHAPING THE FUTURE OF SPORTS SPONSORSHIP</h2>-->
<!--		               <p><span><b>The answer to unified measurement is here.</b></span> Sports Connect delivers the data you need to help inform better business decisions in a best-in-class, user-friendly platform. In a rapidly changing sports market, this solution brings together metrics across media types to deliver a holistic value for all sponsorship assets. <a href="contact" class="contact_text">Contact Us</a><span class="contact_text"> </span></p>-->
			</div>  
        


	</section>

			<!-- icon section -->
			<div class="icon_section">
<!--				<h2>WHAT WE MEASURE</h2>-->
				<div class="icons_holder">
					<div class="icon">
						<div class="icon_circle"><img src="<?=FRONT_ASSETS?>img/tv.png"></div>
						<h3>TV</h3>
					</div>
					<div class="icon">
						<div class="icon_circle"><img src="<?=FRONT_ASSETS?>img/online.png"></div>
						<h3>DIGITAL</h3>
					</div>
					<div class="icon">
						<div class="icon_circle"><img src="<?=FRONT_ASSETS?>img/social.png"></div>
						<h3>SOCIAL</h3>
					</div>
					<div class="icon">
						<div class="icon_circle"><img src="<?=FRONT_ASSETS?>img/print.png"></div>
						<h3>PRINT</h3>
					</div>
				</div>
			</div>
    
			<!-- laptop gif -->
			<div class="image_section">
				<img src="<?=FRONT_ASSETS?>img/screen.gif">
<!--                <a href="#" class="button click_action">TAP TO EXPLORE</a>-->
			</div>

			<!-- One Methodology section -->
			<div class="methodology_section">
                <h2>DASHBOARD</h2>
		        <div class="methodology_holder">
		        	<div class="method">
		        	<img src="<?=FRONT_ASSETS?>img/tv-vs-main.jpg">
                        <a href="#" class="button click_action">TAP TO EXPLORE</a>
		        	</div>
		        </div>
			</div>


			<!-- Power section -->
			<div class="power">
				<h2>VISUAL SEARCH - SOCIAL</h2>
		        <div class="methodology_holder">
		        	<div class="method">
		        	<img src="<?=FRONT_ASSETS?>img/visual-search.png">
                        <a href="#" class="button click_action">TAP TO EXPLORE</a>
		        	</div>
		        </div>
			</div>
            
    			<div class="methodology_section">
                <h2>VISUAL SEARCH - TV</h2>
		        <div class="methodology_holder">
		        	<div class="method">
		        	<img src="<?=FRONT_ASSETS?>img/visual-tv.jpg">
                        <a href="#" class="button click_action">TAP TO EXPLORE</a>
		        	</div>
		        </div>
			</div>
    
    			<div class="power">
				
		        <div class="methodology_holder">
                    
		        	<div class="method">
		        	<img src="<?=FRONT_ASSETS?>img/data-table.png">
                        <a href="#" class="button click_action">TAP TO EXPLORE</a>
		        	</div>
                    <h2>DATA TABLES</h2>
		        </div>
                    		        <div class="methodology_holder">
                        <h2 style="text-align:left">REPORT BUILDER</h2>                
		        	<div class="method">
		        	<img src="<?=FRONT_ASSETS?>img/report-builder.png">
                        <a href="#" class="button click_action">TAP TO EXPLORE</a>
		        	</div>
		        </div>
			</div>

			<!-- Contact section -->
			<div class="footer">
				<h3>Tired of fragmented measurement? Learn more about the power of Sports Connect.</h3>
				<a href="contact" class="button click_action">CONTACT</a>
			</div>
</section>
