<?$questions = $model->questions ?>
<main class="trivia_page">
  <a href='/' class="button click_action back_home">HOME<img src="<?=FRONT_ASSETS?>img/back.png"></a>
  <img class='logo' src="<?=FRONT_ASSETS?>img/logo.png">

  <!-- trivia questions -->
  <section class="trivia_content trivia">
    <div class="title_holder">
      <p>1 / <?=count($questions)?></p>
    </div>

    <!-- question 1 -->
    <?php foreach ($questions as $i => $question) {?>
        <div class="question_holder" data-fail_text="<?=$question->failure_text?>">
            <div class="question">
                <h2>QUESTION <?=$i+1?></h2>
                <p><?=$question->text?></p>
                <? if ($question->image != null && $question->image != '') {?>
                    <img src="<?=UPLOAD_URL . 'questions/' . $question->image ?>" width="100"/>
                <? } ?>
            </div>

            <? $choices = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' ?>

            <?foreach ($question->answers as $a => $answer) {?>
            <div class="answer click_action <?=$question->correct_answer_id == $answer->id ? 'correct_answer' : ''?>">
                <h2><?=substr($choices,$a,1)?></h2>
                <p><?=$answer->text?></p>
                <? if ($answer->featured_image != null && $answer->featured_image != '') {?>
                    <img src="<?=UPLOAD_URL . 'answers/' . $answer->featured_image ?>" width="100"/>
                <? } ?>
            </div>
            <? } ?>
            <a class="button submit">NEXT</a>
        </div>
    <? } ?>
  </section>

  <section class="trivia_completion trivia">
      <div class="title_holder">
        <h1>NIELSEN TRIVIA</h1>
      </div>

      <div class="question">
        <h2></h2>
        <p>Thanks for Playing</p>
      </div>

    </section>
</main>

<script>
    $(document).ready(function () {
          let amount_right = 0
        $(document).on('click', '.answer', function(){

            let failure_text = $(this).parents('.question_holder').data('fail_text');
            
            // if correct answer
            if($(this).hasClass('correct_answer')){ 
              amount_right ++

              // remove ability to choose another answer
                $(this).parents('.question_holder').children('.answer').each(function(){
                    $(this).removeClass('answer click_action');
                    $(this).addClass('answer_outcome');
                });

                var answers = $(this).parent('.question_holder').children('.answer_outcome');
                for ( i=0; i<answers.length; i++ ) {
                  if ( !$(answers[i]).hasClass('correct_answer') ) {
                    $(answers[i]).css('opacity', '0');
                    $(answers[i]).delay(2000).slideUp();
                  }
                }
              
              $(this).after("<div class='answer_outcome correct'> <h2 class='right_answer'>CORRECT</h2></div>");
              $('.correct').slideDown();
              if ( failure_text !== "" ) {
                $(this).after("<div class='answer_outcome correct_info'></div>");
                $('.correct_info').append("<p>" + failure_text + "</p>");
                $('.correct_info').delay(2500).slideDown();
              }

              $(this).parent('.question_holder').children('.submit').delay(2500).fadeIn('fast');
       

            // Wrong answer
            } else { 

                // remove ability to choose another answer
                $(this).parents('.question_holder').children('.answer').each(function(){
                    $(this).removeClass('answer click_action');
                    $(this).addClass('answer_outcome');
                });

                var answers = $(this).parent('.question_holder').children('.answer_outcome');
                for ( i=0; i<answers.length; i++ ) {
                  if ( !$(answers[i]).hasClass('correct_answer') ) {
                    $(answers[i]).css('opacity', '0');
                    $(answers[i]).delay(2000).slideUp();
                  }
                }

                $(this).after("<div class='answer_outcome wrong'> <h2 class='right_answer'>INCORRECT</h2></div>");
                $('.wrong').slideDown();
                    $('.wrong').css('opacity', '0');
                    $('.wrong').delay(2000).slideUp();
                if ( failure_text !== "" ) {
                  $(this).parents('.question_holder').children('.correct_answer').after("<div class='answer_outcome correct_info'></div>");
                  $('.correct_info').append("<p>" + failure_text + "</p>");
                  $('.correct_info').delay(2500).slideDown();
                }

                $(this).parent('.question_holder').children('.submit').delay(2500).fadeIn('fast');
            }

        });


          // Next questions
          var timer;
          var question_num = 1
          var total_questions = $('.title_holder p').html().substring($('.title_holder p').html().length - 2);
          $(".submit").on({
               'click': function clickAction() {
                   var self = this;
                   if ( question_num === parseInt(total_questions)) {
                      $('.trivia_content').css('padding-top', '200px');
                      $('.trivia_content').fadeOut('fast');
                   }else {
                      $(self).parent('.question_holder').css('margin-top', '200px');
                      $(self).parent('.question_holder').fadeOut('slow');
                      console.log(amount_right)
                   }

                    timer = setTimeout(function () {
                      if ( question_num === parseInt(total_questions)) {
                        $('.trivia_completion').fadeIn('slow');
                        $('.trivia_completion').css('padding-top', '0px');
                        console.log(amount_right)
                        $('.trivia_completion .question h2').html(amount_right + " / " + total_questions);
                      }else {
                        $(self).parent('.question_holder').next().fadeIn('slow');
                        $(self).parent('.question_holder').next().css('margin-top', '0px');

                        // Updating question number
                        $('.title_holder p').html((question_num += 1).toString() + "  / " + total_questions )
                      }

                    }, 1000);
               }
          });

         
    });
</script>
