<a href='/' class="button click_action back_home info_home">HOME<img src="<?=FRONT_ASSETS?>img/back_grey.png"></a>  
<img class='logo' src="<?=FRONT_ASSETS?>img/logo.png">
<main class='connect_page'>

	<div class='connect1'>
		<h1>NIELSEN SPORTS CONNECT</h1>
		<p>Your key to unlocking the value of sponsorship across platforms & screens</p>
		<h2>TV</h2>
		<h2>DIGITAL</h2>
		<h2>SOCIAL</h2>
		<h2 class="no_dot">PRINT</h2>
	</div>

	<div class='laptop_holder'>
		<img src="<?=FRONT_ASSETS?>img/screen.gif">
		<button class='start_btn button'>TOUCH TO EXPLORE</button>
	</div>

	<div class='videos'>
		<p class='prompt'>Tap the blue box to proceed</p>
		<img class='vid vid1' src="<?=FRONT_ASSETS?>img/screen1.png">
		<div style='display: none;'>
			<p class='m_info' style='display: none !important;'>i</p>
			<div class='popup'>
				<p class='close' style='display: none !important;'>X</p>
				<p>Instant access to album-level data, granular barcode details, and DMA performance levels.</p>
			</div>
		</div>
		<div class='selector selector1'>
				<img style='margin-top: 100px;' class='move move_select' src="<?=FRONT_ASSETS?>img/hand.png">
		</div>

		<img class='vid vid2' src="<?=FRONT_ASSETS?>img/screen3.png">
		<div>
			<p class='m_info'>i</p>
			<div class='popup'>
				<p class='close'>X</p>
				<h1>ALBUM DASHBOARD</h1>
				<p>Near instantaneous topline and extended views into album-level consumption with complete tracklists, barcode and market level details</p>
			</div>
		</div>
		<div class='selector selector2'>
			<img class='move move_select' src="<?=FRONT_ASSETS?>img/hand.png">
		</div>

		<img class='vid vid3' src="<?=FRONT_ASSETS?>img/screen4.png">
		<div>
			<p class='m_info'>i</p>
			<div class='popup'>
				<p class='close'>X</p>
				<h1>SONG DASHBOARD</h1>
				<p>A deep dive into song-level consumption complete with song and track level details across all markets</p>
			</div>
		</div>
		<div class='selector selector3'>
			<img class='move move_select' src="<?=FRONT_ASSETS?>img/hand.png">
		</div>

		<img class='vid vid4' src="<?=FRONT_ASSETS?>img/screen5.png">
		<div>
			<p class='m_info'>i</p>
			<div class='popup'>
				<p class='close'>X</p>
				<h1>SONG DASHBOARD</h1>
				<p>A deep dive into <strong>song</strong>-level details at your fingertips plus ISRC and market-level data availability.</p>
			</div>
		</div>
		<div class='selector selector4'>
			<img class='move move_select' src="<?=FRONT_ASSETS?>img/hand.png">
		</div>

		<img class='vid vid5' src="<?=FRONT_ASSETS?>img/screen6.png">
		<div>
			<p class='m_info'>i</p>
			<div class='popup'>
				<p class='close'>X</p>
				<h1>ARTIST DASHBOARD</h1>
				<p>A comprehensive look into an Artist discography, inclusive of all album and song activities across all markets</p>
			</div>
		</div>
		<div class='selector selector5'>
			<img class='move move_select' src="<?=FRONT_ASSETS?>img/hand.png">
		</div>

		<p class='contact_text' style='display: none; position: absolute; top: 525px; font-size: 45px;'>To learn more about Music Connect</p>
		<a href="/contact"><button class='button contact_btn'>CONTACT US</button></a>
	</div>


<script type="text/javascript">
	$(document).ready(function(){
		setTimeout(function(){
			$('.laptop').fadeIn(2000);
			$('.laptop').css('margin-top', '0');
		}, 1000);

		setTimeout(function(){
			$('.start_btn').fadeIn();
		}, 3000);

		$('.start_btn').click(function(){
			$(this).css('pointer-events', 'none');
			$('.connect1').fadeOut();
			$('.laptop_holder').fadeOut();
			setTimeout(function(){
				$('.vid1').fadeIn(1500);
				$('.vid1').css('margin-top', '337px');
				$('.selector1').delay(2000).fadeIn(500);
				$('.selector1').prev('div').children('.m_info').delay(2000).fadeIn(500);
				$('.prompt').delay(2000).fadeIn(500);
			}, 1000);
		});


		// all videos
		var timer;
	    $(".selector").on({
	         'click': function clickAction() {
	             var self = this;
	                $(self).css('pointer-events', 'none');
									$(self).prev('div').prev('.vid').fadeOut(500);
									$(self).prev('div').prev('.vid').css('margin-top', '637px');
									$(self).fadeOut();
	              	$(self).prev('div').children('.m_info').fadeOut();
									$('.prompt').fadeOut();

					if ( $(self).next('.vid').hasClass('vid5') ) {
						timer = setTimeout(function () {
		                  $(self).next('.vid').fadeIn(1500);
		                  $(self).next('img').next('div').children('.m_info').delay(2000).fadeIn();
		                  $(self).next('img').next('div').children('.m_info').delay(2000).css('display', 'flex');
											$(self).next('.vid').css('margin-top', '337px');
			            }, 1000);

						setTimeout(function(){
							$('.m_info').fadeOut();
							$('.vid').fadeOut(500);
							$('.vid').css('margin-top', '637px');
							$(self).prev('div').children('.m_info').fadeOut();
							$('.contact_text').delay(1000).fadeIn();
							$('.contact_btn').delay(1000).fadeIn();
						}, 9000);
					}else {
		              timer = setTimeout(function () {
		                  $(self).next('.vid').fadeIn(1500);
		                  $(self).next('img').next('div').children('.m_info').delay(2000).fadeIn();
		                  $(self).next('img').next('div').children('.m_info').delay(2000).css('display', 'flex');
											$(self).next('.vid').css('margin-top', '337px');
											$(self).next('.vid').next('div').next('.selector').delay(3000).fadeIn(500);
							$('.prompt').delay(4000).fadeIn(500);
		              }, 1000);
					}

	         }
	    });


	    $('.m_info').click(function(){
	    	$(this).next('.popup').fadeIn();
	    	$(this).next('.popup').css('display', 'flex');
	    });

	    $('.popup').click(function(){
	    	$('.popup').fadeOut();
	    });
		




	});
</script>

	