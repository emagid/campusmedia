$(document).ready(function(){

    // No Right clicks
    document.addEventListener('contextmenu', event => event.preventDefault());


    // Notification disapear
    setTimeout(function(){
        $('body #in_page_alert_wrapper_nofitication').fadeOut('slow');
    }, 2000);

    // custom valid email statement
    // $(function(){
    //     $("input.contact_form[name=email]")[0].oninvalid = function () {
    //         this.setCustomValidity("Please enter an email name@domain.");
    //         this.setCustomValidity("");
    //     };
    // });

    
    // Choice click
    var timer;
    $(".click_action").on({
         'click': function clickAction() {
             var self = this;
                $(self).css('transform', 'scale(.9)');
              timer = setTimeout(function () {
                  $(self).css('transform', 'scale(1)');
              }, 100);
         }
    });

    <!-- Hotjar Tracking Code for nielsen.mymagid.net -->
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:644766,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');

    // Image alignmnt
    $('.answer').on('click', function(){
        $(this).children('img').css('right', '0');
    });

    $('.answer_outcome').on('click', function(){
        $(this).children('img').css('right', '0');
    });

    $(document).on('click', '.trivia_content .question_holder:nth-of-type(7) .answer', function(){
        $(this).children('img').css('right', '23px');
    });

    $(document).on('click', '.trivia_content .question_holder:nth-of-type(7) .answer_outcome', function(){
        $(this).children('img').css('right', '23px');
    });

    // Trivia click delay
    $('#trivia').on('click', function(e){
        var link = $(this).attr("href");
        e.preventDefault()
        setTimeout(function(){
            $('.home').css('padding-top', '200px');
            $('.home').css('opacity', '0');
            $('.home').delay(500).fadeOut(); 
        }, 500);
        setTimeout(function(){
            window.location.href = link;
        }, 1000);
    });

    // stadium click delay
    $('#stadium').on('click', function(e){
        var link = $(this).attr("href");
        e.preventDefault()
        setTimeout(function(){
            $('.home').css('padding-top', '200px');
            $('.home').css('opacity', '0');
            $('.home').delay(500).fadeOut(); 
        }, 500);
        setTimeout(function(){
            window.location.href = link;
        }, 1000);
    });

    // Contact click delay
     $('#contact').on('click', function(e){
        var link = $(this).attr("href");
        e.preventDefault()
        setTimeout(function(){
            $('.home').css('padding-top', '200px');
            $('.home').css('opacity', '0');
            $('.home').delay(500).fadeOut(); 
        }, 500);
        setTimeout(function(){
            window.location.href = link;
        }, 1000);
    });

    // Info click delay
     $('#info').on('click', function(e){
        var link = $(this).attr("href");
        e.preventDefault()
        setTimeout(function(){
            $('.home').css('padding-top', '200px');
            $('.home').css('opacity', '0');
            $('.home').delay(500).fadeOut(); 
        }, 500);
        setTimeout(function(){
            window.location.href = link;
        }, 1000);
    });

    // Home click delay
     $('.back_home').on('click', function(e){
        var link = $(this).attr("href");
        e.preventDefault()
        setTimeout(function(){
            window.location.href = link;
        }, 500);
    });


    // Page starts
    $('.home').css('opacity', '1');
    $('.trivia_content').fadeIn('fast');
    $('.trivia_content').css('padding-top', '0px');
    $('.panoramic_holder').fadeIn('fast');
    $('.panoramic_holder').css('padding-top', '0px');
    $('.contact').fadeIn('slow');
    $('.contact').css('padding-top', '0px');
    $('.contact_form').fadeIn('slow');
    $('.contact_form').css('padding-top', '0px');
    $('.future_of_sport').fadeIn('slow');


    // Answer click
    $(document).on('click', '.answer', function(){
        $('.answer').css('background-color', 'rgba(255, 255, 255, .80)');
        $(this).css('background-color', 'white');
        // $('.answer').removeClass('chosen');
        // $(this).addClass('chosen');
    });


    // Home animation
    $('.back_home').click(function(){
        $('.back_home img').css('margin-left', '-58px');
    });



    // Rugby Click
    function rugbyClick(){
        $('#soccer').addClass('no_click');
        $('.panoramic').css('background-size', '550%');
        $('.panoramic').css('background-position', 'right -673px bottom');
        $('.pano_large').css('background-size', '550%');
        $('.pano_large').css('background-position', 'right -673px bottom');
        setTimeout(function(){
            $('.panoramic_holder').fadeOut('slow');
            $('.click_points').fadeOut('slow');
        }, 1000);
        setTimeout(function(){
            $('.soccer').css('display', 'none');
            $('.rugby').fadeIn('slow');
            $('.rugby').addClass('active');
            $('.soccer').removeClass('active');
            $('.locator').css('transform','scale(3)');
            setTimeout(function(){
                $('.locator').css('transform','scale(1)');
            }, 1000);
        }, 2000);
        setTimeout(function(){
            $('.click_points').fadeIn('slow');
            $('.panoramic').css('background-image', "none");
        }, 3500);
    }

    $('#rugby').click(rugbyClick);

    // Soccer Click
    function soccerClick(){
        $('#rugby').addClass('no_click');
        $('.panoramic').css('background-size', '550%');
        $('.panoramic').css('background-position', '-1012px bottom');
        $('.pano_large').css('background-size', '550%');
        $('.pano_large').css('background-position', '-1012px bottom');
        setTimeout(function(){
            $('.panoramic_holder').fadeOut('slow');
            $('.click_points').fadeOut('slow');
        }, 1000);
        setTimeout(function(){
            $('.rugby').css('display', 'none');
            $('.soccer').fadeIn('slow');
            $('.soccer').addClass('active');
            $('.rugby').removeClass('active');
            $('.locator').css('transform','scale(3)');
            setTimeout(function(){
                $('.locator').css('transform','scale(1)');
            }, 1000);
        }, 2000);
        setTimeout(function(){
            $('.click_points').fadeIn('slow');
            $('.panoramic').css('background-image', "none");
        }, 3500);
    }

    $('#soccer').click(soccerClick);


    // Back Click
    $('.back_sports').click(function(){
        $('.panoramic').css('background-size', '300%');
        $('.panoramic').css('background-position', 'center'); 
        $('.pano_large').css('background-size', '300%');
        $('.pano_large').css('background-position', 'center'); 
        $('#soccer').removeClass('no_click');  
        $('#rugby').removeClass('no_click');       
        $('.active').fadeOut('slow');
        setTimeout(function(){
            $('.panoramic_holder').fadeIn('slow');
        }, 2000);
    });


    // Scroll indicator
    function loop() {
        $('#scroll').animate({'bottom': '370'}, {
            duration: 1000, 
            complete: function() {
                $('#scroll').animate({bottom: 337}, {
                    duration: 1000, 
                    complete: loop});
            }});        
    }

    loop()


    
    // Arrow fade on info page
    $(window).on('scroll', function() {
       if ( $(window).scrollTop() < 300 ) {
            $('#scroll').fadeIn();
       }else {
            $('#scroll').fadeOut();
       }
   });


    $('#scroll').on('click', function(){
        $('html, body').animate({
                scrollTop: $(".icon_section").offset().top
        }, 2000);
    });


     // Go home after trivia completion
     $('.button.submit').on('click', function(){
        setTimeout(function(){
            if ( $('.trivia_completion').css('display') == 'block' ) {
                $('.trivia_completion').css('padding-top', '200px').fadeOut();
                setTimeout(function(){
                    window.location.replace("/");
                }, 500);
             }
         }, 5000);
     });


     // initial timeout redirect homepage
        var initial = null;

        function invoke() {
            initial = window.setTimeout(
                function() {
                    $('.timeout1').fadeIn();
                }, 60000);
            initial2 = window.setTimeout(
                function() {
                    $('.timeout1').fadeIn();
                }, 600000);
        }


        invoke();
        // invoke2();

        $('body').on('click mousemove', function(){
            window.clearTimeout(initial);
            window.clearTimeout(initial2);
            invoke();
            // invoke2();
        })
         



    // Overlay clicks
    $('.x_box').on('click', function(){
        $('.overlay').fadeOut();
        $('.active').fadeIn();
    });

    $('#adds').on('click', function(){
        $('.active').fadeOut();
        $('#adds_fact').fadeIn();
    });

    $('#ref').on('click', function(){
        $('.active').fadeOut();
        $('#ref_fact').fadeIn();
    });

    $('#jersey').on('click', function(){
        $('.active').fadeOut();
        $('#jersey_fact').fadeIn();
    });

    $('#interview').on('click', function(){
        $('.active').fadeOut();
        $('#interview_fact').fadeIn();
    });

    $('#audience1').on('click', function(){
        $('.active').fadeOut();
        $('#audience1_fact').fadeIn();
    });

    $('#audience2').on('click', function(){
        $('.active').fadeOut();
        $('#audience2_fact').fadeIn();
    });

    $('#soccer_jersey').on('click', function(){
        $('.active').fadeOut();
        $('#soccer_jersey_fact').fadeIn();
    });

    $('#soccer_bench').on('click', function(){
        $('.active').fadeOut();
        $('#soccer_bench_fact').fadeIn();
    });

    $('#audience3').on('click', function(){
        $('.active').fadeOut();
        $('#audience3_fact').fadeIn();
    });

    $('#audience4').on('click', function(){
        $('.active').fadeOut();
        $('#audience4_fact').fadeIn();
    });

    $('#soccer_banner').on('click', function(){
        $('.active').fadeOut();
        $('#soccer_banner_fact').fadeIn();
    });

    $('#scoreboard').on('click', function(){
        $('.active').fadeOut();
        $('#scoreboard_fact').fadeIn();
    });



    // Keyboard input click
    $('.jQKeyboard').on('click', function(){
        $('.jQKeyboard').removeClass('focus');
        $(this).addClass('focus');
    });

    

});



