<?php

namespace Model;

class Entry extends \Emagid\Core\Model {
    static $tablename = "public.entries";

    public static $fields  =  [
        'email' => ['required' => true, 'type' => 'email'],
        'prize',
        'winner',
    ];

    static $prizes = ['$5 GIFT CARD', 'BLACK SUNGLASSES', 'POP SOCKET', 'WHITE SUNGLASSES', '$10 GIFT CARD', 'H&M GOODIE BAG'];

    public static function search($keywords, $limit = 20)
    {
        $sql = "SELECT * FROM entries WHERE active = 1 AND (";
        foreach (explode(' ',urldecode($keywords)) as $keyword){
            if($keyword == '') continue;
            $sql .= " LOWER(email) LIKE LOWER('%$keyword%')";
        }
        $sql .= ") limit " . $limit;
        return self::getList(['sql' => $sql]);
    }
}