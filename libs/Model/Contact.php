<?php

namespace Model;

class Contact extends \Emagid\Core\Model {
    static $tablename = "public.contact";

    public static $fields  =  [
        'name',
        'email',
        'company',
        'message'
    ];
}
























