<?php 

namespace Model; 

class Admin extends \Emagid\Core\Model {
	
	static $tablename = 'administrator'; 

	//static $role = ['0'=>'custom', '1'=>'super', '2'=>'administrator', '3'=>'manager'];

	static $admin_sections =  ['Content', 'Pages', 'Blog', 'Newsletter', 'Questions', 'Users', 'Contacts', 'System', 'Email List', 'Configs', 'Administrators','Entries'];
	
	static $admin_sections_nested = [
			'Content' => ['Pages', 'Blogs', 'Posts', 'Newsletter'],
			'Users' => [],
			'Questions' => [],
			'Entries' => [],
		//'Orders'=>['Shop Orders', 'Session Orders', 'Gift Card Orders'],
			'System' => ['Contacts', 'Configs', ],
			'Administrators' => []
	];
	
	public static $fields =  [
		'first_name'=>[
			'type'=>'text',
			'required'=>true
		], 
		'last_name'=>[
			'type'=>'text',
			'required'=>true,
		],
		'email'=>[
			 'type'=>'email',
			 'required'=>true,
			 'unique'=>true
		],
		'username'=>[
			'type'=>'text',
			'required'=>true,
			'unique'=>true
		],
		'password'=>[
			'type'=>'password',
			'required'=>true,
		],
		'hash', 
		'permissions',
		'signup_ip', 
		'update_time'			
	];
	
	function full_name() {
		return $this->first_name.' ' .$this->last_name;
	}
	
	function is_value_exist($field, $value, $user_id=""){
		if($user_id != ""){
			$where = "{$field} = '{$value}' AND id != {$user_id}";	
		}else{
			$where = "{$field} = '{$value}'";
		}
		if($this->getCount(['where'=>$where]) > 0){
			return true;	
		}else{
			return false;	
		}	
	}
	
	public static function login($username , $password){
		$list = self::getList(['where'=>"username = '{$username}'"]);
		if (count($list)==1) {
			$admin = $list[0];
			$password = \Emagid\Core\Membership::hash($password, $admin->hash, null );
			if ($password['password'] == $admin->password ) {

				$adminRoles = \Model\Admin_Roles::getList(['where' => 'active = 1 and admin_id = '.$admin->id]);
				$rolesIds = [];
				foreach($adminRoles as $role){
					$rolesIds[] = $role->role_id;
				}
				$rolesIds = implode(',', $rolesIds);
				
				$roles = \Model\Role::getList(['where' => 'active = 1 and id in ('.$rolesIds.')']);
	    		$rolesNames = [];
				foreach($roles as $role){
					$rolesNames[] = $role->name;
				}

	    		\Emagid\Core\Membership::setAuthenticationSession($admin->id,$rolesNames, $admin);

				return true;
			}
		}
		return false;
	}
	
	public static function getNestedSections ($permissions = null){
		if (is_null($permissions)){
			return self::$admin_sections_nested;
		} else {
			$permissions = explode(',', $permissions);			
			$sections = self::$admin_sections_nested;
			$authorized = [];
			foreach ($sections as $parent=>$children){
				if (in_array($parent, $permissions)){
					$authorized[$parent] = [];
					foreach ($children as $child){
						if (in_array($child, $permissions)){
							array_push($authorized[$parent], $child);
						}
					}
				}
			}
			return $authorized;
		}
	}
}




